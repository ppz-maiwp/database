<?php

namespace Siza\Database\Models;

use Siza\Database\Models\Spk\Employee;

class User extends Employee
{
    /**
     * @return string
     */
    public function getAvatarAttribute()
    {
        return config('siza-core.url') . '/storage/profile/' . $this->emp_id . '.png';
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return array
     */
    public function getAuthIdentifiersName()
    {
        return ['email', 'user_name', 'emp_id', 'short_name'];
    }
}
