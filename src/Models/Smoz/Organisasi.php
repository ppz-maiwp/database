<?php

namespace Siza\Database\Models\Smoz;

use Siza\Database\Models\AbstractModel;

class Organisasi extends AbstractModel
{
    protected $primaryKey = 'kod_organisasi';

    protected $table = 'smoz_organisasi';

    public $timestamps = false;
}
