<?php

namespace Siza\Database\Models\Kaunter;

use Siza\Database\Models\AbstractModel;
use Siza\Database\Models\Spz\Zakat;

class KutipanKasyerSemasa extends AbstractModel
{
	protected $table = 'vw_dboard_curryr';

	protected $primaryKey = false;

	public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function jenis()
	{
		return $this->belongsTo(Zakat::class, 'jenis_zakat', 'kod_zakat');
	}
}
