<?php

namespace Siza\Database\Models\Zakat2u;

use Siza\Database\Models\AbstractModel as Model;

class ZoCart extends Model
{
    protected $table = 'zo_carts';

    protected $fillable = [
        'total',        // Amount
        'session_id',   // unique ID for each user session
        'user_id',      // foreign key to zo_users table
        'zo_eps_id',    // foreign key to zo_eps table
        'source',       // chatbot, zakat2u
        'processed',    // 0: New record, 1: After user submit to make payment
        'received',     // 0: New record, 1: After received data from ePayment
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function eps()
    {
        return $this->belongsTo(ZoEps::class, 'zo_eps_id', 'eps_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(ZoCartItem::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(ZoUser::class);
    }
}
