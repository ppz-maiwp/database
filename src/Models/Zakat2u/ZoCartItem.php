<?php

namespace Siza\Database\Models\Zakat2u;

use Siza\Database\Models\AbstractModel as Model;

class ZoCartItem extends Model
{
    protected $table = 'zo_cart_items';

    protected $fillable = [
        'type',
        'haul',
        'details',
        'total_yearly',
        'total_monthly',
        'cart_id',
        'kod_zakat',
        'berat_emas'
    ];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function getDetailsAttribute($value)
    {
        return json_decode($value, true);
    }

    public function setDetailsAttribute($value)
    {
        $this->attributes['details'] = json_encode($value);
    }

    public function getJenisZakatTextAttribute()
    {
        foreach (config('constant.zakat.type') as $senarai_zakat) :
            if (array_get($senarai_zakat, 'value') == $this->type) {
                return array_get($senarai_zakat, 'name');
            }
        endforeach;
    }

    public function getTahunHaulAttribute()
    {
        switch ($this->type) {
            case 'FIDYAH':
                return '';

            default:
                return $this->haul;
        }
    }
}
