<?php

namespace Siza\Database\Models\Zakat2u;

use Illuminate\Support\Facades\Cache;
use Siza\Database\App\Http\Helpers\PaymentRecord;
use Siza\Database\Models\AbstractModel;

class ZoUser extends AbstractModel
{
    protected $table = 'zo_users';

    public $timestamps = false;

    /**
     * @return mixed
     */
    public function getAlamatHubungan()
    {
        return $this->alamat_hubungan;
    }

    /**
     * @param mixed $alamat_hubungan
     */
    public function setAlamatHubungan($alamat_hubungan)
    {
        $this->alamat_hubungan = $alamat_hubungan;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->honourific;
    }

    /**
     * @param $honourific
     */
    public function setTitle($honourific)
    {
        $this->honourific = $honourific;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->nama;
    }

    /**
     * @param string $fullname
     */
    public function setFullname($fullname)
    {
        $this->nama = $fullname;
    }

    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;
    }

    /**
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->phone_mobile1;
    }

    /**
     * @param string $mobilePhone
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->phone_mobile1 = $mobilePhone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        if (empty($this->email)) {
            $this->email = $this->email_add;
            $this->save();
        }

        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email_add = $email;
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param int $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getDateLogged()
    {
        return $this->date_logged;
    }

    /**
     * @param mixed $date_logged
     */
    public function setDateLogged($date_logged)
    {
        $this->date_logged = $date_logged;
    }

    /**
     * @param null $format
     * @return mixed
     */
    public function getDateCreated($format = null)
    {
        if (!is_null($format)) {
            return date($format, strtotime($this->date_created));
        }
        return $this->date_created;
    }

    /**
     * @param mixed $date_created
     */
    public function setDateCreated($date_created)
    {
        $this->date_created = $date_created;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIc()
    {
        return $this->username;
    }

    /**
     * @param string $ic
     */
    public function setIc($ic)
    {
        $this->username = $ic;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->alamat_r;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->alamat_r = $address;
    }

    /**
     * @return mixed
     */
    public function getPostcode()
    {
        return $this->poskod_r;
    }

    /**
     * @param mixed $postcode
     */
    public function setPostcode($postcode)
    {
        $this->poskod_r = $postcode;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->bandar2_r;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->bandar2_r = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->negeri_r;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->negeri_r = $state;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getHousePhone()
    {
        return $this->no_tel_r;
    }

    /**
     * @param mixed $housePhone
     */
    public function setHousePhone($housePhone)
    {
        $this->no_tel_r = $housePhone;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->bandar_r;
    }

    /**
     * @param mixed $address2
     */
    public function setAddress2($address2)
    {
        $this->bandar_r = $address2;
    }

    /**
     * @return mixed
     */
    public function getOldIc()
    {
        return $this->nokplama;
    }

    /**
     * @param mixed $oldIc
     */
    public function setOldIc($oldIc)
    {
        $this->nokplama = $oldIc;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMyCard1()
    {
        return $this->mykad1;
    }

    /**
     * @param mixed $myCard1
     */
    public function setMyCard1($myCard1)
    {
        $this->mykad1 = $myCard1;
    }

    /**
     * @return mixed
     */
    public function getMyCard2()
    {
        return $this->mykad2;
    }

    /**
     * @param mixed $myCard2
     */
    public function setMyCard2($myCard2)
    {
        $this->mykad2 = $myCard2;
    }

    /**
     * @return mixed
     */
    public function getMyCard3()
    {
        return $this->mykad3;
    }

    /**
     * @param mixed $myCard3
     */
    public function setMyCard3($myCard3)
    {
        $this->mykad3 = $myCard3;
    }

    /**
     * @return mixed
     */
    public function getMyCard()
    {
        return $this->mykad;
    }

    /**
     * @param mixed $myCard
     */
    public function setMyCard($myCard)
    {
        $this->mykad = $myCard;
    }

    /**
     * @return mixed
     */
    public function getActivationKey()
    {
        return $this->activationkey;
    }

    /**
     * @param mixed $activationKey
     */
    public function setActivationKey($activationKey)
    {
        $this->activationkey = $activationKey;
    }

    /**
     * @return mixed
     */
    public function getPasswordSecure()
    {
        return $this->password_is_secure;
    }

    /**
     * @param mixed $passwordSecure
     */
    public function setPasswordSecure($passwordSecure)
    {
        $this->password_is_secure = $passwordSecure;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }


    /**
     * @return mixed
     */
    public function getIdType()
    {
        return $this->idtype;
    }

    /**
     * @param mixed $idtype
     */
    public function setIdType($idtype)
    {
        $this->idtype = $idtype;
    }

    /**
     * @return mixed
     */
    public function getPasswordHashed()
    {
        $sampah = [
            '10097', 'nothing'
        ];

        if (!empty($this->password) and !in_array($this->password, $sampah)) {
            return $this->password;
        }

        return $this->password_hashed;
    }

    /**
     * @param $pwd
     */
    public function setPasswordHashed($pwd)
    {
        $this->password_hashed = $pwd;
    }

    public function getPaidUserAttribute()
    {
        $user = $this;
        return Cache::remember('user-kutipan-' . md5($this->id), config('siza-core.query_cache'), function () use ($user) {
            $years = PaymentRecord::getYearStartPaid($user);

            return $years === false ?? $years;
        });
    }
}
