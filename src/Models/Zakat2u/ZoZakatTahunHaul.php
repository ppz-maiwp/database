<?php

namespace Siza\Database\Models\Zakat2u;

use Siza\Database\Models\AbstractModel;

class ZoZakatTahunHaul extends AbstractModel
{
    protected $table = 'zo_zakat_tahunhaul';
    protected $primaryKey = 'tahun';
    public $timestamps  = false;
    public $incrementing = false;
}
