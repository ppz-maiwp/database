<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzPenBantuan extends AbstractModel
{
    protected $table = 'spz_penbantuandikaunter';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
