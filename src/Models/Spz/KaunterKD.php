<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterKD extends AbstractModel
{
    protected $table = 'kaunterkd';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
