<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class ClosingItem extends AbstractModel
{
	protected $table = 'closing_master';
	protected $primaryKey = 'koditem';
	public $timestamps = false;
}
