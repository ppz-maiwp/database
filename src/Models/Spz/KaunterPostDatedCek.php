<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterPostDatedCek extends AbstractModel
{
    protected $table = 'kaunterpostdatedcek';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
