<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodPekerjaan extends AbstractModel
{
    protected $table = 'kodpkjaan';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function kodPekerjaan()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }
}
