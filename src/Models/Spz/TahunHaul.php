<?php

namespace Siza\Database\Models\Spz;

use Illuminate\Database\Eloquent\Model;

class TahunHaul extends Model
{
	protected $table = 'zo_zakat_tahunhaul';

	public $timestamps  = false;

	public function setYear($year)
	{
		$this->tahun = $year;
	}

	public function getYear()
	{
		return $this->tahun;
	}

	public function setNisabAmount($amount)
	{
		$this->tahun_nisab = $amount;
	}

	public function getNisabAmount()
	{
		return $this->tahun_nisab;
	}

	public function setGoldAmount($amount)
	{
		$this->hargaemas = $amount;
	}

	public function getGoldAmount()
	{
		return $this->hargaemas;
	}
}
