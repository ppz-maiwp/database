<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKodKumJawatan extends AbstractModel
{
    protected $table = 'SPZ_KODKUMJAWATAN';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];
    public function kodKumJawatan()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }
}
