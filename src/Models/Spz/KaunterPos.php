<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterPos extends AbstractModel
{
    protected $table = 'kaunterpos';
    protected $primaryKey = 'kp_id';
    public $timestamps = false;
}
