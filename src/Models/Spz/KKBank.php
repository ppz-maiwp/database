<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KKBank extends AbstractModel
{
    protected $table = 'kkbank';
    protected $primaryKey = 'kkb_id';
    public $timestamps = false;
}
