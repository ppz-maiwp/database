<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzPtgGajiZakat extends AbstractModel
{
	protected $table = 'spz_ptggajizakat';

	protected $primaryKey = 'idpk';

	protected $fillable = [
		'idpk',
		'idfk',
		'kod_zakat',
		'amaun',
		'amaun_tambahan',
		'amaun_pengurangan',
	];

	public $timestamps = false;
}
