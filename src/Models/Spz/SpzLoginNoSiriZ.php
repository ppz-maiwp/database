<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzLoginNoSiriZ extends AbstractModel
{
    protected $table = 'SPZ_LOGINNOSIRIRZ';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;

    protected $fillable = [
        'ID',
        'CAW',
        'KAUNTER',
        'TKHMASUK',
        'MASAMASUK',
        'USERMASUK',
        'NOMULA',
        'TKHKELUAR',
        'MASAKELUAR',
        'NOAKHIR',
        'NORESITAKHIR',
        'NOAKHIRBELUMGUNA',
    ];
}
