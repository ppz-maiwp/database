<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class VwGetResitPos extends AbstractModel
{
    protected $table = 'vw_get_resitpos';
    //    protected $primaryKey = 'id';
    public $timestamps = false;
}
