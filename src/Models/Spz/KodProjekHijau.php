<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodProjekHijau extends AbstractModel
{
    protected $table = 'kod_projek_hijauweb';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
