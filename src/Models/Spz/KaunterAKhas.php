<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterAKhas extends AbstractModel
{
    protected $table = 'kaunterakhas';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
