<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SizaPenggunaSistem2 extends AbstractModel
{
    protected $table = 'SIZA_PENGGUNASISTEM2';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
