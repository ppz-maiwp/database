<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KBatal extends AbstractModel
{
    protected $table = 'kbatal';
    protected $primaryKey = 'kb_id';
    public $timestamps = false;

    public function sebab_batal()
    {
        return $this->hasOne(SpzSebabBatal::class, 'kb_id', 'kb_id');
    }
}
