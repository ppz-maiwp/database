<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodWarganegara extends AbstractModel
{
    protected $table = 'kodwngr';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function butiranWarganegara()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }

    public function butiranWarganegaraUgat()
    {
        return $this->hasMany('Siza\SPZ\Models\SppznPembayar');
    }
}
