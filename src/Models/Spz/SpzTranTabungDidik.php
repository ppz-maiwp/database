<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzTranTabungDidik extends AbstractModel
{
    protected $table = 'spz_trantabungdidik';
    protected $primaryKey = 'nokplama';
    public $timestamps = false;
}
