<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzDSampulDen extends AbstractModel
{
    protected $table = 'SPZ_DSAMPUL_DEN';
    protected $primaryKey = 'idsampul';
    public $timestamps = false;
}
