<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class TerimaTunai extends AbstractModel
{
    protected $table = 'terimatunai';
    protected $primaryKey = 'ter_tunai_id';
    public $timestamps = false;
}
