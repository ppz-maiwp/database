<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodCawangan extends AbstractModel
{
    protected $table = 'kodcawangan';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string',
    ];

    public function butiranKodCawangan()
    {
        return $this->hasMany(SpiKotakRZ::class);
    }

    public function pc_cawangan()
    {
        return $this->hasMany(PcCawangan::class, 'pc_kodcawangan', 'kod')
            ->where('jenisguna', 1)
            ->orderBy('pc_id', 'asc');
    }
}
