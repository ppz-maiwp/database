<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpiKotakRZ extends AbstractModel
{
    //
    protected $connection = 'siza';
    protected $table = 'SPI_KOTAKRZ';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function butiranKodCawangan()
    {
        return $this->belongsTo('Siza\SPZ\Models\KodCawangan', 'caw', 'kod');
    }
}
