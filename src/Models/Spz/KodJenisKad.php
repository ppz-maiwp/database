<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodJenisKad extends AbstractModel
{
    protected $table = 'ZO_KODJENISKADKREDIT';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
