<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SppznPembayar extends AbstractModel
{
    protected $table = 'Sppzn_pembayar';
    protected $primaryKey = 'NO_K_P_LAMA';
    public $timestamps = false;

    protected $casts = [
        'NO_K_P_LAMA' => 'string',
        'KODB_KOD' => 'string',
    ];

    public function getUgatId()
    {
        //return $this->no_k_p_lama;
        return $this->id;
    }

    public function butiranBangsaUgat()
    {
        return $this->belongsTo('Siza\SPZ\Models\KodBangsa', 'kodb_kod', 'kod');
    }
    public function butiranWarganegaraUgat()
    {
        return $this->belongsTo('Siza\SPZ\Models\KodWarganegara', 'kodw_kod', 'kod');
    }
}
