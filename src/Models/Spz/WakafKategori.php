<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class WakafKategori extends AbstractModel
{
    protected $table = 'wakaf_kodkategori';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
