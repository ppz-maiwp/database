<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzTerimaBantuan extends AbstractModel
{
    protected $table = 'spz_bantuandikaunter';
    protected $primaryKey = 'idpenerima';
    public $timestamps = false;
}
