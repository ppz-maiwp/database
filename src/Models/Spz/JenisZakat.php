<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class JenisZakat extends AbstractModel
{
    protected $table = 'zakat';
    protected $primaryKey = 'kod_zakat';
    public $timestamps = false;

    protected $casts = [
        'kod_zakat' => 'string',
    ];

    public function butiranZakat()
    {
        return $this->hasMany(KJenisZakat::class);
    }
}
