<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzFaktorSebabBatal extends AbstractModel
{
    protected $table = 'spz_faktorsebabbatal';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
