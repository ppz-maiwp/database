<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class WakafKutipanJenisWakaf extends AbstractModel
{
    protected $table = 'wakaf_kutipan_jeniswakaf';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
