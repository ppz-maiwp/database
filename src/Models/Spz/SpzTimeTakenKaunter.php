<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzTimeTakenKaunter extends AbstractModel
{
    protected $table = 'spz_timetakenkaunter';
    protected $primaryKey = 'kut_id';
    public $timestamps = false;
}
