<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterMM extends AbstractModel
{
    protected $table = 'kauntermm';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
