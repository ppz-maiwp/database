<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;

class KodBayaran extends Model
{
    protected $table = 'kodbayaran';

    protected $primaryKey = 'kod_cara';
}
