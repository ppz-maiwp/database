<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodPJenis extends AbstractModel
{
    protected $table = 'kod_pjenis';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];
    public function jenisTerperinci()
    {
        return $this->hasMany(Pbayar::class);
    }
}
