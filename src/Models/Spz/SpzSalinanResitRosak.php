<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzSalinanResitRosak extends AbstractModel
{
    protected $table = 'spz_salinanresitr';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
