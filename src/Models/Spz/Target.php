<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class Target extends AbstractModel
{
	protected $table = 'target';
	protected $primaryKey = 'tar_id';
	public $timestamps = false;
}
