<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class EmasM extends AbstractModel
{
    protected $table = 'emas_m';
    protected $primaryKey = 'emm_id';
    public $timestamps = false;
}
