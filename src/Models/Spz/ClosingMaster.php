<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class ClosingMaster extends AbstractModel
{
	protected $table = 'closing_master';
	protected $primaryKey = false;
	public $timestamps = false;

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function items()
	{
		return $this->hasMany(ClosingItem::class, 'koditem', 'koditem')
			->where('groupid', 1)
			->where('jenisitem', 'T');
	}
}
