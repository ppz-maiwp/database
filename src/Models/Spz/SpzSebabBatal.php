<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzSebabBatal extends AbstractModel
{
	protected $table = 'spz_sebabbatal';
	protected $primaryKey = 'kb_id';
	public $timestamps = false;

	public function kod_sebab_batal()
	{
		return $this->hasOne(SpzKodSebabBatal::class, 'sebabbatal', 'kod');
	}
}
