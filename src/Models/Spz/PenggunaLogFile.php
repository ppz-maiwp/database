<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class PenggunaLogFile extends AbstractModel
{
    protected $table = 'pengguna_logfile';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
