<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodCaraBayar extends AbstractModel
{
    protected $table = 'kcarabyr';
    protected $primaryKey = 'kcb_id';
    public $timestamps = false;

    /* protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];*/
}
