<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzDaftarPreProsesIn extends AbstractModel
{
    protected $table = 'spz_daftarpreproses_in';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /*  protected $casts = [
        'kod' => 'string'
    ];*/
}
