<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;

class VwKadarNisab extends Model
{
	protected $table = 'vw_kadarnisab';

	protected $primaryKey = null;
}
