<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KPGaji extends AbstractModel
{
    protected $table = 'kpgaji';
    protected $primaryKey = 'kpg_id';
    public $timestamps = false;
}
