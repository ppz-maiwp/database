<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class EventPgaji extends AbstractModel
{
    protected $table = 'v2_event_pgaji';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
