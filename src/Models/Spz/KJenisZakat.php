<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;

class KJenisZakat extends Model
{
    protected $table = 'kjeniszkt';

    protected $primaryKey = 'kjz_id';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function zakat()
    {
        return $this->belongsTo(Zakat::class, 'zkt_kod_zakat', 'kod_zakat');
    }
}
