<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class TerimaCek extends AbstractModel
{
    protected $table = 'terimacek';
    protected $primaryKey = 'cek_id';
    public $timestamps = false;

    protected $casts = [
        'cek_id' => 'integer'
    ];
}
