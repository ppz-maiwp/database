<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterAgen extends AbstractModel
{
    protected $table = 'kaunteragen';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
