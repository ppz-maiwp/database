<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpiKotakSRZ extends AbstractModel
{
    protected $table = 'spi_kotaksrz';
    //    protected $primaryKey = 'id';
    public $timestamps = false;
}
