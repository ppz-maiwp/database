<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodKadBank extends AbstractModel
{
    protected $table = 'ZO_KODBANKKADKREDIT';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
