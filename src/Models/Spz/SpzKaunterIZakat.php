<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKaunterIZakat extends AbstractModel
{
    protected $table = 'spz_kaunterizakat';
    //    protected $primaryKey = 'id';
    public $timestamps = false;
}
