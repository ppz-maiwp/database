<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodBangsa extends AbstractModel
{
    protected $table = 'kodbangsa';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function butiranBangsa()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }

    public function butiranBangsaUgat()
    {
        return $this->hasMany(SppznPembayar::class);
    }
}
