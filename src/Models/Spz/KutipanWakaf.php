<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KutipanWakaf extends AbstractModel
{
    protected $table = 'wakaf_kutipan_m';
    protected $primaryKey = 'id';
    public $timestamps = false;

    /*protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];*/
}
