<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzNoSiriSalinanErr extends AbstractModel
{
    protected $table = 'spz_nosiris_er';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
