<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterUniv extends AbstractModel
{
    protected $table = 'spz_kaunteruniversiti';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
