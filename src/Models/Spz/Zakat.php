<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;

class Zakat extends Model
{
    protected $table = 'zakat';

    protected $primaryKey = 'kod_zakat';
}
