<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzDSampul extends AbstractModel
{
    protected $table = 'spz_dsampul';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
