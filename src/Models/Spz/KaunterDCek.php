<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterDCek extends AbstractModel
{
    protected $table = 'kaunterdcek';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
