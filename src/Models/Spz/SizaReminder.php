<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SizaReminder extends AbstractModel
{
    protected $table = 'siza_reminder';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;

    protected $casts = [
        'PUNCAUPDATE' => 'interger',
    ];
}
