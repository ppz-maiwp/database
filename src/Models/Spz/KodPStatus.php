<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodPStatus extends AbstractModel
{
    protected $table = 'kod_pstatus';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function butiranStatus()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }
}
