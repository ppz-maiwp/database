<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterKK extends AbstractModel
{
    protected $table = 'kaunterkk';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
