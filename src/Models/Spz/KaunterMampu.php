<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterMampu extends AbstractModel
{
    protected $table = 'kauntermampu';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
