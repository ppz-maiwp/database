<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;

class SpzKaunterPortalPpz extends Model
{
    protected $table = 'spz_kaunterportalppz';

    public $timestamps = false;
}
