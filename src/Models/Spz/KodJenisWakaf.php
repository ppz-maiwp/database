<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodJenisWakaf extends AbstractModel
{
    protected $table = 'wakaf_kodjeniswakaf';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
