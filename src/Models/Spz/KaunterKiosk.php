<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterKiosk extends AbstractModel
{
    protected $table = 'kaunterkiosk';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
