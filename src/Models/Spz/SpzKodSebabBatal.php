<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKodSebabBatal extends AbstractModel
{
    protected $table = 'spz_kodsebabbatal';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
