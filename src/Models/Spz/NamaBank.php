<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class NamaBank extends AbstractModel
{
    protected $table = 'bank';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
