<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpkSalutation extends AbstractModel
{
    protected $table = 'V2_Spk_Salutation';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function butiranGelaran()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }
}
