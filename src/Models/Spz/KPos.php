<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KPos extends AbstractModel
{
    protected $table = 'kpos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
