<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KodPJenisK extends AbstractModel
{
    protected $table = 'kod_pjenis_k';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    protected $casts = [
        'kod' => 'string'
    ];

    public function jenisTerperinci()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }
}
