<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class V2_SkzMohonBatal extends AbstractModel
{
    protected $table = 'v2_skz_mohonbtl';
    protected $primaryKey = 'idmohon';
    public $timestamps = false;
}
