<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKodPuncaResit extends AbstractModel
{
    protected $table = 'spz_kodpuncaposresit';
    protected $primaryKey = null;
    public $timestamps = false;
}
