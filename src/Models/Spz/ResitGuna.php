<?php

namespace Siza\Database\Models\Spz;

use Baum\Extensions\Eloquent\Model;
use Siza\Database\Models\AbstractModel;

class ResitGuna extends Model
{
    protected $table = 'resit';
    protected $primaryKey = 'NO_RESIT';
    public $timestamps = false;

    protected $fillable = [
        'no_siri'
    ];

    protected $casts = [
        //        'NO_RESIT' => 'string',
        //        'KODB_KOD' => 'string',
        'no_siri' => 'integer'
    ];

    public function getTarikhProsesAttribute()
    {
        if (empty($this->tkh_proses)) {
            return '';
        }

        return date('d/m/Y', strtotime($this->tkh_proses));
    }
}
