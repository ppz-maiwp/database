<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKaunterAutoDebit extends AbstractModel
{
    protected $table = 'spz_kaunterautodebit';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
