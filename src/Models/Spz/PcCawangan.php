<?php

namespace Siza\Database\Models\Spz;

use Carbon\Carbon;
use Siza\Monitoring\Models\KaunterStatus;
use Siza\Database\Models\AbstractModel;

class PcCawangan extends AbstractModel
{
	protected $table = 'pc_cawangan';
	protected $primaryKey = 'pc_id';
	public $timestamps = false;

	protected $casts = [
		'pc_id' => 'string',
	];

	public function getStatusAttribute()
	{
		$statuses = $this->statuses();

		if (count($statuses)) {
			return 1;
		}

		return 0;
	}

	public function statuses()
	{
		return $this->hasMany(KaunterStatus::class, 'name', 'pc_id')
			->where('last_ping' <= Carbon::now()->subMinutes(5)->toDateTimeString());
	}

	public function cawangan()
	{
		return $this->belongsTo(KodCawangan::class, 'pc_kodcawangan', 'kod');
	}
}
