<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class V2MasihiHijrah extends AbstractModel
{
    protected $table = 'v2_tahun_hijri_masihi';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
