<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class Mesej extends AbstractModel
{
    protected $table = 'mesej';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
