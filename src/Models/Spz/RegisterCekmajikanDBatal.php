<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class RegisterCekmajikanDBatal extends AbstractModel
{
    protected $table = 'register_cekmajikan_d_batal';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
