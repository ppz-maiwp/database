<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\User;
use Siza\Database\Models\AbstractModel as Model;
use Siza\Database\Models\Zakat2u\ZoEps;

class Resit extends Model
{
    protected $table = 'resit';

    protected $primaryKey = 'no_resit';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function kutipan()
    {
        return $this->hasOne(Kutipan::class, 'kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kJenisZakat()
    {
        return $this->hasMany(KJenisZakatUbah::class, 'kut_kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kCaraBayar()
    {
        return $this->hasMany(KCaraBayar::class, 'kut_kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class, 'username', 'pby_no_k_p_lama');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user2()
    {
        return $this->hasOne(User::class, 'nokplama', 'pby_no_k_p_lama');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function zoeps()
    {
        return $this->hasOne(ZoEps::class, 'kut_id', 'kut_kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function preproses()
    {
        return $this->hasOne(DaftarPreProses::class, 'noresit', 'no_resit');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function spzKaunterPortalPpz()
    {
        return $this->hasOne(SpzKaunterPortalPpz::class, 'kut_id', 'kut_kut_id');
    }

    /**
     * @return string
     */
    public function getResitBaruAttribute()
    {
        return $this->kodresit2 . $this->noresit2;
    }
}
