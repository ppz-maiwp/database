<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class Ugat extends AbstractModel
{
    protected $table = 'ugat';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
