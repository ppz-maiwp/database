<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KaunterMajlis extends AbstractModel
{
    protected $table = 'kauntermajlis';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
