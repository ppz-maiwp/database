<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzResitdanNoSementara extends AbstractModel
{
    protected $table = 'spz_resitdannosementara';
    protected $primaryKey = null;
    public $incrementing = false;
    public $timestamps = false;
}
