<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class RegisterCekMajikan extends AbstractModel
{
    protected $table = 'REGISTER_CEKMAJIKAN';
    protected $primaryKey = 'no_rujukan';
    public $timestamps = false;
}
