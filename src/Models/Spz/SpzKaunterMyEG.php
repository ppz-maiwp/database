<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKaunterMyEG extends AbstractModel
{
    protected $table = 'spz_kauntermyeg';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
