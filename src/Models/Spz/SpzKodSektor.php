<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKodSektor extends AbstractModel
{
    protected $table = 'SPZ_kodsektor';
    protected $primaryKey = 'kod';
    public $timestamps = false;

    //    protected $casts = [
    //        'kod' => 'string'
    //    ];
    public function kodSektor()
    {
        return $this->hasMany('Siza\SPZ\Models\Pbayar');
    }
}
