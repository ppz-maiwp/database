<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzKodKaunterUniversiti extends AbstractModel
{
    protected $table = 'spz_kodkaunteruniversiti';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
