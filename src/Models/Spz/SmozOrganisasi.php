<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SmozOrganisasi extends AbstractModel
{
    protected $table = 'smoz_organisasi';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
