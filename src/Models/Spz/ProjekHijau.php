<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class ProjekHijau extends AbstractModel
{
    protected $table = 'pr_emelhijau_web';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
