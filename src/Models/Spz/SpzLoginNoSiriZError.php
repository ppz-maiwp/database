<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzLoginNoSiriZError extends AbstractModel
{
    protected $table = 'spz_loginnosirirz_er';
    protected $primaryKey = 'idm';
    public $timestamps = false;
    protected $casts = [
        'idm' => 'integer',
        'kodsalah' => 'integer',
        'idowner' => 'integer',
    ];
}
