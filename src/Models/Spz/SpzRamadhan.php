<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzRamadhan extends AbstractModel
{
    protected $table = 'spz_tempohramadan';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
