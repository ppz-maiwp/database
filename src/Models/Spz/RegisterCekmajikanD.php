<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class RegisterCekmajikanD extends AbstractModel
{
    protected $table = 'register_cekmajikan_d';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
