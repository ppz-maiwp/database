<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class WakafCaraBayar extends AbstractModel
{
    protected $table = 'wakaf_kutipan_carabayar';
    public $timestamps = false;

    /* protected $casts = [
        'kodcaraterima' => 'integer',
        'jenisprogram' => 'integer',
    ];*/
}
