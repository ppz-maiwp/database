<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;

class Pbayar extends Model
{
    protected $table = 'pbayar';

    protected $primaryKey = 'no_k_p_lama';

    protected $fillable = [
        'no_k_p_lama', 'nama', 'first_name', 'last_name', 'e_mel'
    ];

    public $timestamps = false;

    public $incrementing = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kutipan()
    {
        return $this->hasMany(Kutipan::class, 'pby_no_k_p_lama', 'no_k_p_lama');
    }

    public function majikan()
    {
        return $this->belongsTo(Majikan::class, 'maj_kod_majikan', 'kod_majikan');
    }

    /**
     * @return mixed
     */
    public function getNOKPLAMA()
    {
        return $this->NO_K_P_LAMA;
    }

    /**
     * @param mixed $NO_K_P_LAMA
     */
    public function setNOKPLAMA($NO_K_P_LAMA)
    {
        $this->NO_K_P_LAMA = $NO_K_P_LAMA;
    }

    /**
     * @return mixed
     */
    public function getKODPCKOD()
    {
        return $this->KOD_PC_KOD;
    }

    /**
     * @param mixed $KOD_PC_KOD
     */
    public function setKODPCKOD($KOD_PC_KOD)
    {
        $this->KOD_PC_KOD = $KOD_PC_KOD;
    }

    /**
     * @return mixed
     */
    public function getKODPJKOD()
    {
        return $this->KOD_PJ_KOD;
    }

    /**
     * @param mixed $KOD_PJ_KOD
     */
    public function setKODPJKOD($KOD_PJ_KOD)
    {
        $this->KOD_PJ_KOD = $KOD_PJ_KOD;
    }

    /**
     * @return mixed
     */
    public function getKODPKKOD()
    {
        return $this->KODPK_KOD;
    }

    /**
     * @param mixed $KODPK_KOD
     */
    public function setKODPKKOD($KODPK_KOD)
    {
        $this->KODPK_KOD = $KODPK_KOD;
    }

    /**
     * @return mixed
     */
    public function getMAJKODMAJIKAN()
    {
        return $this->MAJ_KOD_MAJIKAN;
    }

    /**
     * @param mixed $MAJ_KOD_MAJIKAN
     */
    public function setMAJKODMAJIKAN($MAJ_KOD_MAJIKAN)
    {
        $this->MAJ_KOD_MAJIKAN = $MAJ_KOD_MAJIKAN;
    }

    /**
     * @return mixed
     */
    public function getPYPPYPID()
    {
        return $this->PYP_PYP_ID;
    }

    /**
     * @param mixed $PYP_PYP_ID
     */
    public function setPYPPYPID($PYP_PYP_ID)
    {
        $this->PYP_PYP_ID = $PYP_PYP_ID;
    }

    /**
     * @return mixed
     */
    public function getKODNKOD()
    {
        return $this->KODN_KOD;
    }

    /**
     * @param mixed $KODN_KOD
     */
    public function setKODNKOD($KODN_KOD)
    {
        $this->KODN_KOD = $KODN_KOD;
    }

    /**
     * @return mixed
     */
    public function getKODBKOD()
    {
        return $this->KODB_KOD;
    }

    /**
     * @param mixed $KODB_KOD
     */
    public function setKODBKOD($KODB_KOD)
    {
        $this->KODB_KOD = $KODB_KOD;
    }

    /**
     * @return mixed
     */
    public function getKODWKOD()
    {
        return $this->KODW_KOD;
    }

    /**
     * @param mixed $KODW_KOD
     */
    public function setKODWKOD($KODW_KOD)
    {
        $this->KODW_KOD = $KODW_KOD;
    }

    /**
     * @return mixed
     */
    public function getKODPSKOD()
    {
        return $this->KOD_PS_KOD;
    }

    /**
     * @param mixed $KOD_PS_KOD
     */
    public function setKODPSKOD($KOD_PS_KOD)
    {
        $this->KOD_PS_KOD = $KOD_PS_KOD;
    }

    /**
     * @return mixed
     */
    public function getNOKPBARU()
    {
        return $this->NO_K_P_BARU;
    }

    /**
     * @param mixed $NO_K_P_BARU
     */
    public function setNOKPBARU($NO_K_P_BARU)
    {
        $this->NO_K_P_BARU = $NO_K_P_BARU;
    }

    /**
     * @return mixed
     */
    public function getNAMA()
    {
        return $this->NAMA;
    }

    /**
     * @param mixed $NAMA
     */
    public function setNAMA($NAMA)
    {
        $this->NAMA = $NAMA;
    }

    /**
     * @return mixed
     */
    public function getGELARAN()
    {
        return $this->GELARAN;
    }

    /**
     * @param mixed $GELARAN
     */
    public function setGELARAN($GELARAN)
    {
        $this->GELARAN = $GELARAN;
    }

    /**
     * @return mixed
     */
    public function getALAMATR()
    {
        return $this->ALAMAT_R;
    }

    /**
     * @param mixed $ALAMAT_R
     */
    public function setALAMATR($ALAMAT_R)
    {
        $this->ALAMAT_R = $ALAMAT_R;
    }

    /**
     * @return mixed
     */
    public function getPOSKODR()
    {
        return $this->POSKOD_R;
    }

    /**
     * @param mixed $POSKOD_R
     */
    public function setPOSKODR($POSKOD_R)
    {
        $this->POSKOD_R = $POSKOD_R;
    }

    /**
     * @return mixed
     */
    public function getBANDARR()
    {
        return $this->BANDAR_R;
    }

    /**
     * @param mixed $BANDAR_R
     */
    public function setBANDARR($BANDAR_R)
    {
        $this->BANDAR_R = $BANDAR_R;
    }

    /**
     * @return mixed
     */
    public function getNEGERIR()
    {
        return $this->NEGERI_R;
    }

    /**
     * @param mixed $NEGERI_R
     */
    public function setNEGERIR($NEGERI_R)
    {
        $this->NEGERI_R = $NEGERI_R;
    }

    /**
     * @return mixed
     */
    public function getNOTELR()
    {
        return $this->NO_TEL_R;
    }

    /**
     * @param mixed $NO_TEL_R
     */
    public function setNOTELR($NO_TEL_R)
    {
        $this->NO_TEL_R = $NO_TEL_R;
    }

    /**
     * @return mixed
     */
    public function getALAMATP()
    {
        return $this->ALAMAT_P;
    }

    /**
     * @param mixed $ALAMAT_P
     */
    public function setALAMATP($ALAMAT_P)
    {
        $this->ALAMAT_P = $ALAMAT_P;
    }

    /**
     * @return mixed
     */
    public function getPOSKODP()
    {
        return $this->POSKOD_P;
    }

    /**
     * @param mixed $POSKOD_P
     */
    public function setPOSKODP($POSKOD_P)
    {
        $this->POSKOD_P = $POSKOD_P;
    }

    /**
     * @return mixed
     */
    public function getBANDARP()
    {
        return $this->BANDAR_P;
    }

    /**
     * @param mixed $BANDAR_P
     */
    public function setBANDARP($BANDAR_P)
    {
        $this->BANDAR_P = $BANDAR_P;
    }

    /**
     * @return mixed
     */
    public function getNEGERIP()
    {
        return $this->NEGERI_P;
    }

    /**
     * @param mixed $NEGERI_P
     */
    public function setNEGERIP($NEGERI_P)
    {
        $this->NEGERI_P = $NEGERI_P;
    }

    /**
     * @return mixed
     */
    public function getNOTELP()
    {
        return $this->NO_TEL_P;
    }

    /**
     * @param mixed $NO_TEL_P
     */
    public function setNOTELP($NO_TEL_P)
    {
        $this->NO_TEL_P = $NO_TEL_P;
    }

    /**
     * @return mixed
     */
    public function getALAMATHUBUNGAN()
    {
        return $this->ALAMAT_HUBUNGAN;
    }

    /**
     * @param mixed $ALAMAT_HUBUNGAN
     */
    public function setALAMATHUBUNGAN($ALAMAT_HUBUNGAN)
    {
        $this->ALAMAT_HUBUNGAN = $ALAMAT_HUBUNGAN;
    }

    /**
     * @return mixed
     */
    public function getJANTINA()
    {
        return $this->JANTINA;
    }

    /**
     * @param mixed $JANTINA
     */
    public function setJANTINA($JANTINA)
    {
        $this->JANTINA = $JANTINA;
    }

    /**
     * @return mixed
     */
    public function getTKHLAHIR()
    {
        return $this->TKH_LAHIR;
    }

    /**
     * @param mixed $TKH_LAHIR
     */
    public function setTKHLAHIR($TKH_LAHIR)
    {
        $this->TKH_LAHIR = $TKH_LAHIR;
    }

    /**
     * @return mixed
     */
    public function getAKTIF()
    {
        return $this->AKTIF;
    }

    /**
     * @param mixed $AKTIF
     */
    public function setAKTIF($AKTIF)
    {
        $this->AKTIF = $AKTIF;
    }

    /**
     * @return mixed
     */
    public function getCATATAN()
    {
        return $this->CATATAN;
    }

    /**
     * @param mixed $CATATAN
     */
    public function setCATATAN($CATATAN)
    {
        $this->CATATAN = $CATATAN;
    }

    /**
     * @return mixed
     */
    public function getEMEL()
    {
        return $this->E_MEL;
    }

    /**
     * @param mixed $E_MEL
     */
    public function setEMEL($E_MEL)
    {
        $this->E_MEL = $E_MEL;
    }

    /**
     * @return mixed
     */
    public function getNOHP()
    {
        return $this->NO_HP;
    }

    /**
     * @param mixed $NO_HP
     */
    public function setNOHP($NO_HP)
    {
        $this->NO_HP = $NO_HP;
    }

    /**
     * @return mixed
     */
    public function getNEGERIKERJA()
    {
        return $this->NEGERI_KERJA;
    }

    /**
     * @param mixed $NEGERI_KERJA
     */
    public function setNEGERIKERJA($NEGERI_KERJA)
    {
        $this->NEGERI_KERJA = $NEGERI_KERJA;
    }

    /**
     * @return mixed
     */
    public function getNOTEL2R()
    {
        return $this->NO_TEL2_R;
    }

    /**
     * @param mixed $NO_TEL2_R
     */
    public function setNOTEL2R($NO_TEL2_R)
    {
        $this->NO_TEL2_R = $NO_TEL2_R;
    }

    /**
     * @return mixed
     */
    public function getNOFAXP()
    {
        return $this->NO_FAX_P;
    }

    /**
     * @param mixed $NO_FAX_P
     */
    public function setNOFAXP($NO_FAX_P)
    {
        $this->NO_FAX_P = $NO_FAX_P;
    }

    /**
     * @return mixed
     */
    public function getBANDAR2P()
    {
        return $this->BANDAR2_P;
    }

    /**
     * @param mixed $BANDAR2_P
     */
    public function setBANDAR2P($BANDAR2_P)
    {
        $this->BANDAR2_P = $BANDAR2_P;
    }

    /**
     * @return mixed
     */
    public function getBANDAR2R()
    {
        return $this->BANDAR2_R;
    }

    /**
     * @param mixed $BANDAR2_R
     */
    public function setBANDAR2R($BANDAR2_R)
    {
        $this->BANDAR2_R = $BANDAR2_R;
    }

    /**
     * @return mixed
     */
    public function getKODMKOD()
    {
        return $this->KODM_KOD;
    }

    /**
     * @param mixed $KODM_KOD
     */
    public function setKODMKOD($KODM_KOD)
    {
        $this->KODM_KOD = $KODM_KOD;
    }

    /**
     * @return mixed
     */
    public function getTKHMASUK()
    {
        return $this->TKH_MASUK;
    }

    /**
     * @param mixed $TKH_MASUK
     */
    public function setTKHMASUK($TKH_MASUK)
    {
        $this->TKH_MASUK = $TKH_MASUK;
    }

    /**
     * @return mixed
     */
    public function getOWNERID()
    {
        return $this->OWNERID;
    }

    /**
     * @param mixed $OWNERID
     */
    public function setOWNERID($OWNERID)
    {
        $this->OWNERID = $OWNERID;
    }

    /**
     * @return mixed
     */
    public function getEMEL2()
    {
        return $this->E_MEL2;
    }

    /**
     * @param mixed $E_MEL2
     */
    public function setEMEL2($E_MEL2)
    {
        $this->E_MEL2 = $E_MEL2;
    }

    /**
     * @return mixed
     */
    public function getKUMPPEKERJAAN()
    {
        return $this->KUMPPEKERJAAN;
    }

    /**
     * @param mixed $KUMPPEKERJAAN
     */
    public function setKUMPPEKERJAAN($KUMPPEKERJAAN)
    {
        $this->KUMPPEKERJAAN = $KUMPPEKERJAAN;
    }

    /**
     * @return mixed
     */
    public function getTKHKEMASKINI()
    {
        return $this->TKH_KEMASKINI;
    }

    /**
     * @param mixed $TKH_KEMASKINI
     */
    public function setTKHKEMASKINI($TKH_KEMASKINI)
    {
        $this->TKH_KEMASKINI = $TKH_KEMASKINI;
    }

    /**
     * @return mixed
     */
    public function getUSERKEMASKINI()
    {
        return $this->USER_KEMASKINI;
    }

    /**
     * @param mixed $USER_KEMASKINI
     */
    public function setUSERKEMASKINI($USER_KEMASKINI)
    {
        $this->USER_KEMASKINI = $USER_KEMASKINI;
    }

    /**
     * @return mixed
     */
    public function getKODORGANISASI()
    {
        return $this->KOD_ORGANISASI;
    }

    /**
     * @param mixed $KOD_ORGANISASI
     */
    public function setKODORGANISASI($KOD_ORGANISASI)
    {
        $this->KOD_ORGANISASI = $KOD_ORGANISASI;
    }

    /**
     * @return mixed
     */
    public function getSEKTOR()
    {
        return $this->SEKTOR;
    }

    /**
     * @param mixed $SEKTOR
     */
    public function setSEKTOR($SEKTOR)
    {
        $this->SEKTOR = $SEKTOR;
    }

    /**
     * @return mixed
     */
    public function getKUMPULANJENIS()
    {
        return $this->KUMPULANJENIS;
    }

    /**
     * @param mixed $KUMPULANJENIS
     */
    public function setKUMPULANJENIS($KUMPULANJENIS)
    {
        $this->KUMPULANJENIS = $KUMPULANJENIS;
    }

    /**
     * @return mixed
     */
    public function getUSERMASUK()
    {
        return $this->USER_MASUK;
    }

    /**
     * @param mixed $USER_MASUK
     */
    public function setUSERMASUK($USER_MASUK)
    {
        $this->USER_MASUK = $USER_MASUK;
    }

    /**
     * @return mixed
     */
    public function getKODNEGERIP()
    {
        return $this->KODNEGERI_P;
    }

    /**
     * @param mixed $KODNEGERI_P
     */
    public function setKODNEGERIP($KODNEGERI_P)
    {
        $this->KODNEGERI_P = $KODNEGERI_P;
    }

    /**
     * @return mixed
     */
    public function getKODNEGERIR()
    {
        return $this->KODNEGERI_R;
    }

    /**
     * @param mixed $KODNEGERI_R
     */
    public function setKODNEGERIR($KODNEGERI_R)
    {
        $this->KODNEGERI_R = $KODNEGERI_R;
    }
}
