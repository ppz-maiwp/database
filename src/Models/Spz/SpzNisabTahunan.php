<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzNisabTahunan extends AbstractModel
{
    protected $table = 'spz_nisabtahunan';
    protected $primaryKey = null;
    public $timestamps = false;
}
