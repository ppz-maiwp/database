<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class KKaunter extends AbstractModel
{
    protected $table = 'kkaunter';
    protected $primaryKey = 'kk_id';
    public $timestamps = false;
}
