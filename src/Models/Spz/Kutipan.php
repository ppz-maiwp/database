<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;
use Siza\Database\Models\User;

class Kutipan extends Model
{
    protected $table = 'kutipan';
    protected $primaryKey = 'kut_id';
    public $timestamps  = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pbayar()
    {
        return $this->belongsTo(PBayar::class, 'pby_no_k_p_lama',  'no_k_p_lama');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function caraBayar()
    {
        return $this->hasMany(KCaraBayar::class, 'kut_kut_id', 'kut_id')
            ->where('kodbyr_kod_cara', '!=', 7);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jenisZakat()
    {
        return $this->hasMany(KJenisZakat::class, 'kut_kut_id', 'kut_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'pby_no_k_p_lama', 'username');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function resit()
    {
        return $this->hasOne(Resit::class, 'kut_kut_id', 'kut_id');
    }

    /**
     * @return string
     */
    public function getNoResitBaruAttribute()
    {
        return $this->resit->kodresit2 . $this->resit->noresit2;
    }

    /**
     * @return mixed
     */
    public function getNamaMajikanAttribute()
    {
        return $this->pbayar->majikan->nama_majikan;
    }

    /**
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @param string $primaryKey
     */
    public function setPrimaryKey($primaryKey)
    {
        $this->primaryKey = $primaryKey;
    }

    /**
     * @return mixed
     */
    public function getKUTID()
    {
        return $this->KUT_ID;
    }

    /**
     * @param mixed $KUT_ID
     */
    public function setKUTID($KUT_ID)
    {
        $this->KUT_ID = $KUT_ID;
    }

    /**
     * @return mixed
     */
    public function getTKHKUTIPAN()
    {
        return $this->TKH_KUTIPAN;
    }

    /**
     * @param mixed $TKH_KUTIPAN
     */
    public function setTKHKUTIPAN($TKH_KUTIPAN)
    {
        $this->TKH_KUTIPAN = $TKH_KUTIPAN;
    }

    /**
     * @return mixed
     */
    public function getMASAKUTIPAN()
    {
        return $this->MASA_KUTIPAN;
    }

    /**
     * @param mixed $MASA_KUTIPAN
     */
    public function setMASAKUTIPAN($MASA_KUTIPAN)
    {
        $this->MASA_KUTIPAN = $MASA_KUTIPAN;
    }

    /**
     * @return mixed
     */
    public function getJUMLAH()
    {
        return $this->JUMLAH;
    }

    /**
     * @param mixed $JUMLAH
     */
    public function setJUMLAH($JUMLAH)
    {
        $this->JUMLAH = $JUMLAH;
    }

    /**
     * @return mixed
     */
    public function getJENISTRANSAKSI()
    {
        return $this->JENIS_TRANSAKSI;
    }

    /**
     * @param mixed $JENIS_TRANSAKSI
     */
    public function setJENISTRANSAKSI($JENIS_TRANSAKSI)
    {
        $this->JENIS_TRANSAKSI = $JENIS_TRANSAKSI;
    }

    /**
     * @return mixed
     */
    public function getKASKODKASYER()
    {
        return $this->KAS_KOD_KASYER;
    }

    /**
     * @param mixed $KAS_KOD_KASYER
     */
    public function setKASKODKASYER($KAS_KOD_KASYER)
    {
        $this->KAS_KOD_KASYER = $KAS_KOD_KASYER;
    }

    /**
     * @return mixed
     */
    public function getKODCWKOD()
    {
        return $this->KOD_CW_KOD;
    }

    /**
     * @param mixed $KOD_CW_KOD
     */
    public function setKODCWKOD($KOD_CW_KOD)
    {
        $this->KOD_CW_KOD = $KOD_CW_KOD;
    }

    /**
     * @return mixed
     */
    public function getPBYNOKPLAMA()
    {
        return $this->PBY_NO_K_P_LAMA;
    }

    /**
     * @param mixed $PBY_NO_K_P_LAMA
     */
    public function setPBYNOKPLAMA($PBY_NO_K_P_LAMA)
    {
        $this->PBY_NO_K_P_LAMA = $PBY_NO_K_P_LAMA;
    }

    /**
     * @return mixed
     */
    public function getJENISTRANS()
    {
        return $this->JENIS_TRANS;
    }

    /**
     * @param mixed $JENIS_TRANS
     */
    public function setJENISTRANS($JENIS_TRANS)
    {
        $this->JENIS_TRANS = $JENIS_TRANS;
    }

    /**
     * @return mixed
     */
    public function getOLDRECPTNO()
    {
        return $this->OLD_RECPTNO;
    }

    /**
     * @param mixed $OLD_RECPTNO
     */
    public function setOLDRECPTNO($OLD_RECPTNO)
    {
        $this->OLD_RECPTNO = $OLD_RECPTNO;
    }

    /**
     * @return mixed
     */
    public function getFLAGBTL()
    {
        return $this->FLAG_BTL;
    }

    /**
     * @param mixed $FLAG_BTL
     */
    public function setFLAGBTL($FLAG_BTL)
    {
        $this->FLAG_BTL = $FLAG_BTL;
    }

    /**
     * @return mixed
     */
    public function getOWNERID()
    {
        return $this->OWNERID;
    }

    /**
     * @param mixed $OWNERID
     */
    public function setOWNERID($OWNERID)
    {
        $this->OWNERID = $OWNERID;
    }

    /**
     * @return mixed
     */
    public function getNAMAPC()
    {
        return $this->NAMAPC;
    }

    /**
     * @param mixed $NAMAPC
     */
    public function setNAMAPC($NAMAPC)
    {
        $this->NAMAPC = $NAMAPC;
    }

    /**
     * @return mixed
     */
    public function getNORESIT()
    {
        return $this->NORESIT;
    }

    /**
     * @param mixed $NORESIT
     */
    public function setNORESIT($NORESIT)
    {
        $this->NORESIT = $NORESIT;
    }

    /**
     * @return mixed
     */
    public function getKODCARATERIMA()
    {
        return $this->KODCARATERIMA;
    }

    /**
     * @param mixed $KODCARATERIMA
     */
    public function setKODCARATERIMA($KODCARATERIMA)
    {
        $this->KODCARATERIMA = $KODCARATERIMA;
    }

    /**
     * @return mixed
     */
    public function getJENISPROGRAM()
    {
        return $this->JENISPROGRAM;
    }

    /**
     * @param mixed $JENISPROGRAM
     */
    public function setJENISPROGRAM($JENISPROGRAM)
    {
        $this->JENISPROGRAM = $JENISPROGRAM;
    }
}
