<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class RpLogfile extends AbstractModel
{
    protected $table = 'rp_logfile';
    protected $primaryKey = 'rp_id';
    public $timestamps = false;

    /*protected $casts = [
        'PUNCAUPDATE' => 'interger',
    ];*/
}
