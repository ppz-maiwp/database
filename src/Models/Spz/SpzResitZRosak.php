<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SpzResitZRosak extends AbstractModel
{
    protected $table = 'spz_resitrzrosak';
    //    protected $primaryKey = 'kod_majikan';
    public $timestamps = false;
}
