<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel;

class SuratPos extends AbstractModel
{
    protected $table = 'surat_pos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
