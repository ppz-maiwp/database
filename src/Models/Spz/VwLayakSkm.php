<?php

namespace Siza\Database\Models\Spz;

use Siza\Database\Models\AbstractModel as Model;

class VwLayakSkm extends Model
{
    protected $table = 'vw_layak_skm';

    protected $primaryKey = false;

    public $timestamps = false;
}
