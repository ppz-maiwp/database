<?php

namespace Siza\Database\Models\Kalkulator;

use Siza\Database\Models\AbstractModel;

class RekodKiraanSimpanan extends AbstractModel
{
    protected $table = 'zo_kiraan_simpanan';

    protected $fillable = [
        'tahun_haul',
        'nilai_nisab',
        'jumlah_simpanan',
        'jumlah_layak_zakat',
        'jumlah_zakat_setahun',
        'jumlah_zakat_sebulan',
        'senarai_simpanan',
        'user_id'
    ];
}
