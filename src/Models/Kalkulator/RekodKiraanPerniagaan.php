<?php

namespace Siza\Database\Models\Kalkulator;

use Siza\Database\Models\AbstractModel;

class RekodKiraanPerniagaan extends AbstractModel
{
    protected $table = 'zo_kiraan_niaga';

    protected $fillable = [
        'nisab',
        'tahun_haul',
        'nilai_nisab',
        'kaedah_taksiran',
        'nama_syarikat',
        'no_pendaftaran',
        'alamat',
        'no_telefon',
        'aset_semasa',
        'liabiliti_semasa',
        'stok_bahan_mentah',
        'penghutang',
        'deposit_tetap',
        'hutang_dari_syarikat_induk',
        'simpanan_tetap',
        'b_lain_lain_1_text',
        'b_lain_lain_1_value',
        'b_lain_lain_2_text',
        'b_lain_lain_2_value',
        'b_lain_lain_3_text',
        'b_lain_lain_3_value',
        'b_lain_lain_4_text',
        'b_lain_lain_4_value',
        'pemiutang',
        'overdraf',
        'dividen',
        'hutang_pada_syarikat_induk',
        'sewa_beli',
        'peruntukan_zakat',
        'c_lain_lain_1_text',
        'c_lain_lain_1_value',
        'c_lain_lain_2_text',
        'c_lain_lain_2_value',
        'c_lain_lain_3_text',
        'c_lain_lain_3_value',
        'c_lain_lain_4_text',
        'c_lain_lain_4_value',
        'peratus_muslim',
        'jumlah_bahagian_a',
        'jumlah_bahagian_b',
        'jumlah_bahagian_c',
        'jumlah_bahagian_d',
        'jumlah_lebihan_aset',
        'jumlah_layak_zakat',
        'user_id',
    ];

    public function getJenisKiraanAttribute()
    {
        if ($this->kaedah_taksiran === 'modalkerja') {
            return 'Modal Kerja';
        }
        return 'Untung Rugi';
    }
}
