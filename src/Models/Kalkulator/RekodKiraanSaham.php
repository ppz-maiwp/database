<?php

namespace Siza\Database\Models\Kalkulator;

use Siza\Database\Models\AbstractModel;

class RekodKiraanSaham extends AbstractModel
{
    protected $fillable = [
        'nisab',
        'jenis_zakat',
        'tahun_haul',
        'nilai_nisab',
        'jenis',
        'jumlah_harga_pasaran',
        'baki_akaun',
        'kos',
        'jumlah_layak_zakat',
        'jumlah_zakat_setahun',
        'jumlah_zakat_sebulan',
        'senarai_saham',
        'user_id',
        'jumlah_saham'
    ];

    protected $table = 'zo_kiraan_saham';

    public function getJenisTextAttribute()
    {
        switch ($this->jenis) {
            case 1:
                return 'Patuh Syariah';

            default:
                return 'Tidak Patuh Syariah';
        }
    }
}
