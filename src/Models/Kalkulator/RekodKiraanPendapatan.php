<?php

namespace Siza\Database\Models\Kalkulator;

use Siza\Database\Models\AbstractModel;

class RekodKiraanPendapatan extends AbstractModel
{
	protected $table = 'zo_kiraan_pendapatan';

	protected $fillable = [
		'tahunHaul', 'nisab', 'jenis_zakat', 'direct', 'tahun_haul', 'nilai_nisab', 'jenis_kiraan', 'jenis_pendapatan',
		'pendapatan_bulanan', 'pendapatan_tahunan', 'pendapatan_lain', 'jumlah_pendapatan', 'belanja_sendiri',
		'jumlah_isteri', 'jumlah_belanja_isteri', 'amaun_belanja_isteri', 'jumlah_anak', 'jumlah_belanja_anak',
		'amaun_belanja_anak', 'jumlah_anak2', 'jumlah_belanja_anak2', 'amaun_belanja_anak2', 'jumlah_belanja_ibubapa',
		'jenis_kwsp', 'nilai_kwsp', 'jumlah_kwsp', 'jumlah_pendidikan_sendiri', 'pendidikan_sendiri_max',
		'jumlah_perbelanjaan', 'jumlah_layak_zakat', 'caruman_zakat', 'jumlah_caruman_zakat', 'jumlah_zakat_setahun',
		'jumlah_zakat_sebulan', 'user_id'
	];

	protected $casts = [
		'nisab' => 'float',
		'direct' => 'integer',
		'tahun_haul' => 'integer',
		'nilai_nisab' => 'float',
		'jenis_kiraan' => 'integer',
		'jenis_pendapatan' => 'integer',
		'pendapatan_bulanan' => 'float',
		'pendapatan_tahunan' => 'float',
		'pendapatan_lain' => 'float',
		'jumlah_pendapatan' => 'float',
		'belanja_sendiri' => 'float',
		'jumlah_isteri' => 'integer',
		'jumlah_belanja_isteri' => 'float',
		'amaun_belanja_isteri' => 'float',
		'jumlah_anak' => 'integer',
		'jumlah_belanja_anak' => 'float',
		'amaun_belanja_anak' => 'float',
		'jumlah_anak2' => 'integer',
		'jumlah_belanja_anak2' => 'float',
		'amaun_belanja_anak2' => 'float',
		'jumlah_belanja_ibubapa' => 'float',
		'jenis_kwsp' => 'string',
		'nilai_kwsp' => 'float',
		'jumlah_kwsp' => 'float',
		'jumlah_pendidikan_sendiri' => 'float',
		'pendidikan_sendiri_max' => 'float',
		'jumlah_perbelanjaan' => 'float',
		'jumlah_layak_zakat' => 'float',
		'caruman_zakat' => 'float',
		'jumlah_caruman_zakat' => 'float',
		'jumlah_zakat_setahun' => 'float',
		'jumlah_zakat_sebulan' => 'float',
	];

	public function getJenisKiraanTextAttribute()
	{
		switch ((int) $this->jenis_kiraan) {
			case 1:
				return 'Dengan Tolakan';

			default:
				return 'Tanpa Tolakan';
		}
	}
}
