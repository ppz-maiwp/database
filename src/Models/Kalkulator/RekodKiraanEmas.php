<?php

namespace Siza\Database\Models\Kalkulator;

use Siza\Database\Models\AbstractModel;

class RekodKiraanEmas extends AbstractModel
{
    protected $table = 'zo_kiraan_emas';

    protected $fillable = [
        'jumlah_emas_pakai', 'jumlah_emas_simpan', 'jumlah_zakat_setahun', 'jumlah_zakat_sebulan', 'senarai_kiraan', 'user_id'
    ];
}
