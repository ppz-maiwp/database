<?php

namespace Siza\Database\Models\Kalkulator;

use Siza\Database\Models\AbstractModel;

class RekodKiraanKwsp extends AbstractModel
{
	protected $table = 'zo_kiraan_kwsp';

	protected $fillable = [
		'tahunHaul', 'nisab', 'jenis_zakat', 'direct', 'tahun_haul', 'jumlah_keluaran', 'nilai_nisab', 'user_id',
		'jumlah_layak_zakat', 'jumlah_zakat_setahun', 'jumlah_zakat_sebulan',
	];

	protected $casts = [
		'nisab' => 'float',
		'direct' => 'integer',
		'tahun_haul' => 'integer',
		'nilai_nisab' => 'float',
		'jumlah_keluaran' => 'integer',
		'jumlah_layak_zakat' => 'float',
		'jumlah_zakat_setahun' => 'float',
		'jumlah_zakat_sebulan' => 'float',
		'user_id' => 'integer',
	];
}
