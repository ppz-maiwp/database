<?php

namespace Siza\Database\Models;

class NamaNegeri extends AbstractModel
{
    protected $table = 'namanegeri';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
