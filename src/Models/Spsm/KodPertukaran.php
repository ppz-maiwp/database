<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodPertukaran extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_pertukaran';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kod' => 'integer'
    ];

    public $timestamps = false;

    public function pertukaran()
    {
        return $this->hasMany(Pertukaran::class);
    }
}
