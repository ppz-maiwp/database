<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class EmployeeDetail extends AbstractModel
{
    protected $primaryKey = false;

    protected $table = 'spsm_employee_detail';

    //    protected $fillable = [
    //        'alamat',
    //        'alamat_t',
    //        'amaun_rebatpcb',
    //        'bandar',
    //        'bandar1_t',
    //        'bandar2',
    //        'bandar2_t',
    //        'bank_cawangan',
    //        'cukaikenabayar_g',
    //        'cukai_cukai_g',
    //        'cukai_zakat_g',
    //        'email',
    //        'emp_id',
    //        'gajipencen_bulan',
    //        'gajipencen_tahun',
    //        'gratuiti',
    //        'jantina',
    //        'kategori_pcb',
    //        'kod_bank',
    //        'kod_group_kaunter',
    //        'kumpulan',
    //        'kwsp_kategori',
    //        'kwsp_p',
    //        'lhdn_susunan',
    //        'negeri',
    //        'negeri_t',
    //        'no_akaun_bank',
    //        'no_kwsp',
    //        'no_k_p_baru',
    //        'no_k_p_lama',
    //        'no_lhdn',
    //        'no_perkeso',
    //        'no_tel',
    //        'no_tel2',
    //        'no_tel_sambungan',
    //        'peratus_zakat',
    //        'poskod',
    //        'poskod_t',
    //        'rebatpcb_individu',
    //        'status_oku',
    //        'taraf_perkahwinan',
    //        'tkhkontrakmula',
    //        'tkhkontrakmula2',
    //        'tkhkontraktamat',
    //        'tkhkontraktamat2',
    //        'tkh_jawatan_tetap',
    //        'tkh_lahir',
    //        'tkh_mula_khidmat',
    //        'tkh_sah_jawatan',
    //        'url',
    //    ];

    public $timestamps = false;

    protected $dates = [
        'tkh_mula_khidmat',
        'tkh_sah_jawatan',
        'tkh_jawatan_tetap',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank()
    {
        return $this->belongsTo(KodBank::class, 'kod_bank', 'kod');
    }
}
