<?php

namespace Siza\Database\Models\Spsm;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Siza\Database\Models\AbstractModel;

class Employee extends AbstractModel
{
    protected $primaryKey = 'emp_id';

    protected $table = 'spsm_employee';

    protected $fillable = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'emp_id' => 'string',
        'grade_id' => 'integer',
    ];

    public $timestamps = false;

    public function scopeActive($query)
    {
        return $query
            ->where('resign', null)
            ->where('jawatan_id', '!=', null)
            ->where('unit_id', '!=', null)
            ->where('dept_id', '!=', null);
    }

    public function getNameAttribute()
    {
        return $this->emp_name;
    }

    public function getTitleAttribute()
    {
        return $this->emp_title;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function detail()
    {
        return $this->hasOne(EmployeeDetail::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function maklumatPeribadi()
    {
        return $this->detail();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(KodDept::class, 'dept_id', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bahagian()
    {
        return $this->department();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function designation()
    {
        return $this->belongsTo(KodJawatan::class, 'jawatan_id', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jawatan()
    {
        return $this->designation();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(EmployeeImage::class, 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function pengguna()
    {
        return $this->hasOne(Pengguna::class, 'emp_id');
    }

    /**
     * @return string
     */
    public function getAvatarUrlAttribute()
    {
        $filename = storage_path('public/profile/' . $this->emp_id . '.png');

        try {
            if (! file_exists($filename)) {
                $image = EmployeeImage::where('emp_id', $this->emp_id)
                    ->first();

                if ($image AND ! is_null($image->gambar)) {
                    File::put($filename, $image->gambar);
                }
            }

        } catch (\Exception $e) {
            Log::error($e->getMessage());
        }

        return Storage::url($filename);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kelasPekerja()
    {
        return $this->belongsTo(KelasPekerja::class, 'kumpulan', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gred()
    {
        return $this->belongsTo(KodGrade::class, 'grade_id', 'kod');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(KodUnit::class, 'unit_id', 'kod');
    }
}
