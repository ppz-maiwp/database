<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class Pertukaran extends AbstractModel
{
    protected $primaryKey = 'pertukaran_id';

    protected $table = 'spsm_pertukaran';

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kod' => 'integer',
        'grade' => 'string',
    ];

    public $timestamps = false;

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function unit()
    {
        return $this->hasOne(KodUnit::class, 'kod', 'unit_id_b');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function bahagian()
    {
        return $this->hasOne(KodDept::class, 'kod', 'dept_id_b');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function jawatan()
    {
        return $this->hasOne(KodJawatan::class, 'kod', 'jawatan_id_b');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gred()
    {
        return $this->hasOne(KodGrade::class, 'kod', 'grade_id_b');
    }
}
