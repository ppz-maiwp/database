<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodGrade extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_grade';

    //    protected $fillable = [
    //        'kod', 'butiran', 'grade', 'pindaan', 'tahunpindaan'
    //    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kod' => 'integer',
        'grade' => 'string',
    ];

    public $timestamps = false;

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
