<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KelasPekerja extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kelaspekerja';

    public $timestamps = false;

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
