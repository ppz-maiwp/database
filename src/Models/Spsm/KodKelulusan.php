<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodKelulusan extends AbstractModel
{
    protected $table = 'spsm_kodkelulusan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    protected $casts = [
        'butiran' => 'string',
    ];
}
