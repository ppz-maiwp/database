<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KhususKelulusan extends AbstractModel
{
    protected $table = 'spsm_khususkelulusan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    protected $casts = [
        'butiran' => 'string',
    ];
}
