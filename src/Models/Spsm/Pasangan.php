<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class Pasangan extends AbstractModel
{
    protected $primaryKey = 'idpasangan';

    protected $table = 'spsm_pasangan';

    protected $fillable = [
        'emp_id',
        'emp_id_pasangan',
        'nama_pasangan',
        's_no_kp_lama',
        's_no_kp_baru',
        's_tkh_lahir',
        's_kod_organisasi',
        'status_sah',
        's_tkh_kahwin',
        'kodpk_kod',
        'cukai',
        'no_lhdn_pasangan',
        'kod_organisasi',
        'jawatan',
        'no_tel_r',
        'no_hp',
        'statuspasangan',
        'jum_pendapatanbulanan',
        'jum_kwsp',
        'jum_perkeso',
        'jum_cukaipendapatan',
        'jum_pinjamankenderaan',
        'jum_pinjamanperumahan',
        'jum_lain',
        'telmajikan',
        'tkhstatus',
        'idpasangan',
        'status_oku',
        'rebat_tax',
        'bil_isteri',
    ];

    protected $casts = [
        's_tkh_lahir' => 'date',
        's_tkh_kahwin' => 'date',
    ];

    public $timestamps = false;

    public function getTarikhLahirAttribute()
    {
        //        return date('d/m/Y', strtotime($this->s_tkh_lahir));
        return $this->s_tkh_lahir;
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }
}
