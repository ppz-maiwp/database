<?php

namespace Siza\Database\Models\Spsm;

use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Siza\Database\Models\AbstractModel;

class EmployeeMlain extends AbstractModel
{
    protected $primaryKey = false;

    protected $table = 'spsm_employee_mlain';

    public $timestamps = false;

    //    protected $hidden = array(
    //        'alamatmajikan_b',
    //        'alamatmajikan_i',
    //        'alamat_b',
    //        'alamat_i',
    //        'bandar1_b',
    //        'bandar1_i',
    //        'bandar2majikan_b',
    //        'bandar2majikan_i',
    //        'bandar2_b',
    //        'bandar2_i',
    //        'bandarmajikan_b',
    //        'bandarmajikan_i',
    //        'emp_id',
    //        'kpbarubapa',
    //        'kpbaruibu',
    //        'kplamabapa',
    //        'kplamaibu',
    //        'namabapa',
    //        'namaibu',
    //        'namamajikan_b',
    //        'namamajikan_i',
    //        'negerimajikan_b',
    //        'negerimajikan_i',
    //        'negeri_b',
    //        'negeri_i',
    //        'poskodmajikan_b',
    //        'poskodmajikan_i',
    //        'poskod_b',
    //        'poskod_i',
    //        'statusbapa',
    //        'statusibu',
    //        'telbapa',
    //        'telibu',
    //        'telmajikan_b',
    //        'telmajikan_i',
    //        'tkhmati_bapa',
    //        'tkhmati_ibu',
    //        'tkh_lahirbapa',
    //        'tkh_lahiribu',
    //    );

    protected $casts = [
        'tkh_lahiribu' => 'date',
        'tkh_lahirbapa' => 'date',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function getTkhLahirIbuAttribute($value)
    {
        return date('d/m/Y', strtotime($value));
    }
}
