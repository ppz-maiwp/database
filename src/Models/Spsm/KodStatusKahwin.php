<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodStatusKahwin extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_status_kahwin';

    public $timestamps = false;
}
