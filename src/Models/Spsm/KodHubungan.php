<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodHubungan extends AbstractModel
{
    protected $table = 'spsm_kodhubungan';
    protected $primaryKey = 'kod';
    public $timestamps = false;
}
