<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodUnit extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_unit';

    public $timestamps = false;

    protected $casts = [
        'kod' => 'integer',
        'butiran' => 'string',
        'kod_papar' => 'string',
        'status' => 'integer',
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
