<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodDept extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_dept';

    public $timestamps = false;

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
