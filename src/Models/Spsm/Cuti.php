<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class Cuti extends AbstractModel
{
    protected $primaryKey = 'cuti_id';

    protected $table = 'spsm_cuti';
}
