<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodJenisJawatan extends AbstractModel
{
    protected $table = 'spsm_kodjenisjawatan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    protected $casts = [
        'butiran' => 'string',
    ];
}
