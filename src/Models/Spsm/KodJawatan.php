<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodJawatan extends AbstractModel
{
    protected $table = 'spsm_kod_jawatan';

    protected $primaryKey = 'kod';

    public $timestamps = false;

    protected $casts = [
        'butiran' => 'string',
    ];

    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
