<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class DaftarGl extends AbstractModel
{
    protected $table = 'spsm_daftargl';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
