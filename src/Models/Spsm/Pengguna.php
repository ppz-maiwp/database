<?php

namespace Siza\Database\Models\Spsm;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Siza\Database\Models\AbstractModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Pengguna extends AbstractModel implements AuthenticatableContract
{
    use Authenticatable;

    protected $primaryKey = 'emp_id';

    protected $table = 'pengguna';

    public $timestamps = false;

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function getAuthIdentifierName()
    {
        return 'emp_id';
    }

    public function getAuthIdentifier()
    {
        return $this->emp_id;
    }

    public function getNameAttribute()
    {
        return $this->employee->emp_name;
    }

    /**
     * @return string
     */
    public function getAvatarAttribute()
    {
        return Storage::url('public/profile/' . $this->emp_id . '.png');
    }

    /**
     * Get tempoh berkhidmat
     *
     * @return array
     */
    public function tempohBerkhidmat()
    {
        $now = Carbon::today();
        $start = $this->employee->detail->tkh_sah_jawatan;

        $months = $now->diffInMonths($start);

        $years = floor($months / 12);

        $yearDiff = $years * 12;

        $months -= $yearDiff;

        return [
            'tahun' => $years,
            'bulan' => $months
        ];
    }
}
