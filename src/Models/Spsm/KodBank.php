<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodBank extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kod_bank';

    public $timestamps = false;
}
