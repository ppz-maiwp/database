<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class CutiLayak extends AbstractModel
{
    protected $primaryKey = false;

    protected $table = 'spsm_cuti_layak';
}
