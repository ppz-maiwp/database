<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class KodStatusJawatan extends AbstractModel
{
    protected $primaryKey = 'kod';

    protected $table = 'spsm_kodstatusjawatan';

    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
}
