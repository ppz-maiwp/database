<?php

namespace Siza\Database\Models\Spsm;

use Siza\Database\Models\AbstractModel;

class EmployeePendidikan extends AbstractModel
{
    protected $primaryKey = 'id';

    protected $table = 'spsm_employee_pend';

    public $timestamps = false;

    //    protected $fillable = array(
    //        'emp_id',
    //        'tahundari',
    //        'tahunhingga',
    //        'kelulusan',
    //        'institusi',
    //        'catatan',
    //        'khusus',
    //        'jumlahyuran',
    //    );

    protected $casts = [
        'id' => 'integer',
        'tahundari' => 'integer',
        'tahunhingga' => 'integer',
        'kelulusan' => 'integer',
        'khusus' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class, 'emp_id', 'emp_id');
    }

    public function jenis_kelulusan()
    {
        return $this->belongsTo(KodKelulusan::class, 'kelulusan', 'kod');
    }

    public function jenis_khusus()
    {
        return $this->belongsTo(KhususKelulusan::class, 'khusus', 'kod');
    }

    public function organisasi()
    {
        return $this->belongsTo(SMOZ_Organisasi::class, 'institusi', 'kod_organisasi');
    }

    public function getYuranAttribute()
    {
        if (empty($this->jumlahyuran) or is_null($this->jumlahyuran)) {
            return '0.00';
        }

        return number_format($this->jumlahyuran, 2);
    }
}
