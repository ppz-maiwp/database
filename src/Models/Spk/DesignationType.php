<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class DesignationType
 * @package Siza\Database\Models\Spk
 * @deprecated
 */
class DesignationType extends AbstractModel
{
    protected $table = 'v2_spk_designation_types';

    protected $fillable = [
        'id', 'name'
    ];
}
