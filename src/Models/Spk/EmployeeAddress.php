<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class EmployeeAddress
 * @package Siza\Database\Models\Spk
 * @deprecated
 */
class EmployeeAddress extends AbstractModel
{
    protected $table = 'v2_spk_employee_addresses';

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
