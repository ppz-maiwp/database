<?php

namespace Siza\Database\Models\Spk;

class Role extends \Kodeine\Acl\Models\Eloquent\Role
{
	protected $table = 'v2_spk_roles';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = 'v2_spk_roles';
    }

    /**
     * Use the slug of the Role
     * instead of the ID.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'id';
    }

	/**
	 * Roles can belong to many users.
	 *
	 */
	public function users()
	{
		return $this->belongsToMany(config('auth.providers.users.model', config('auth.model')), 'v2_spk_role_user', 'role_id', 'employee_id')
			->withTimestamps();
	}

	/**
	 * Roles can belong to many permissions.
	 *
	 */
	public function permissions()
	{
		return $this->belongsToMany(Permission::class, 'v2_spk_permission_role', 'role_id', 'permission_id')
			->withTimestamps();
	}
}
