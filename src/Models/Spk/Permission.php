<?php

namespace Siza\Database\Models\Spk;

class Permission extends \Kodeine\Acl\Models\Eloquent\Permission
{
	protected $table = 'v2_spk_permissions';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->table = 'v2_spk_permissions';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(Employee::class, 'v2_spk_permission_user', 'permission_id', 'employee_id')
            ->withTimestamps();
    }

	/**
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function roles()
	{
		$model = config('acl.role', Role::class);

		return $this->belongsToMany($model, 'v2_spk_permission_role', 'permission_id', 'role_id')
			->withTimestamps();
	}

	/**
	 * @param $value
	 * @return array
	 */
	public function getSlugAttribute($value)
	{
		return $value;
	}

	/**
	 * @param $value
	 */
	public function setSlugAttribute($value)
	{
		// store as json.
		$this->attributes['slug'] = $value;
	}
}
