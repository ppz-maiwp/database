<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\Spsm\KodJawatan;
use Siza\Database\Models\AbstractModel;

/**
 * Class Employment
 * @package Siza\Database\Models\Spk
 * @deprecated  Use Siza\Database\Models\Spsm\Pertukaran
 */
class Employment extends AbstractModel
{
    protected $table = 'v2_spk_employments';

    protected $fillable = [
        'id',
        'emp_id',
        'grade_id',
        'unit_id',
        'department_id',
        'designation_id',
        'designation_type_id',
        'employment_type_id',
        'employment_reason_id',
        'added_by_id',
        'started_at',
        'ended_at',
        'created_at',
        'updated_at',
        'probation',
        'resign'
    ];

    protected $casts = [
        'employee_id' => 'integer',
        'employment_type_id' => 'integer',
        'unit_id' => 'integer',
        'department_id' => 'integer',
        'grade_id' => 'integer',
        'designation_id' => 'integer',
        'added_by_id' => 'integer',
        'resign' => 'integer',
        'problem' => 'integer',
        'probation' => 'integer',
        'grade_id_b' => 'integer',
        'jenisjawatan_b' => 'integer',
        'jenispekerja_b' => 'integer',
    ];

    public $timestamps = false;

    /**
     * Who's the actor
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    /**
     * Which Department?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    /**
     * Which Unit?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unit()
    {
        return $this->belongsTo(Unit::class);
    }

    /**
     * What is the Grade?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function grade()
    {
        return $this->belongsTo(Grade::class);
    }

    /**
     * What is the Designation?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

    /**
     * What is the Reason adding the record?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employment_type()
    {
        return $this->belongsTo(EmploymentType::class);
    }

    /**
     * What is the employment type
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo(EmploymentType::class);
    }

    /**
     * Added by who?
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function addedBy()
    {
        return $this->belongsTo(Employee::class, 'added_by_id', 'id');
    }

    public function jenis_jawatan_l()
    {
        return $this->belongsTo(KodJawatan::class, 'jenisjawatan_l', 'kod');
    }
}
