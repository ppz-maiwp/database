<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class Unit
 * @package Siza\Database\Models\Spk
 * @deprecated  Use Siza\Database\Models\Spsm\KodUnit
 */
class Unit extends AbstractModel
{
    protected $table = 'v2_spk_units';

    protected $fillable = [
        'name',
        'initials',
        'code',
        'ordering',
        'status',
        'manager_id',
        'scheme_id',
    ];

    protected $casts = [
        'manager_id' => 'integer',
        'ordering' => 'integer',
        'status' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manager()
    {
        return $this->belongsTo(Employee::class, 'manager_id');
    }
}
