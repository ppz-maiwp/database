<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class Grade
 * @package Siza\Database\Models\Spk
 * @deprecated  Use Siza\Database\Models\Spsm\KodGrade
 */
class Grade extends AbstractModel
{
    protected $table = 'v2_spk_grades';

    protected $fillable = [
        'id',
        'name',
        'grade',
        'qualification',
        'level',
        'amendment',
        'amendment_month',
        'amendment_year',
        'status',
        'scheme_id',
    ];

    public function employments()
    {
        return $this->hasMany(Employment::class);
    }
}
