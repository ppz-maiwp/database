<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class EmployeeSpouse
 * @package Siza\Database\Models\Spk
 * @deprecated  Use Siza\Database\Models\Spsm\Pasangan
 */
class EmployeeSpouse extends AbstractModel
{
    protected $table = 'spsm_pasangan';

    protected $fillable = [
        'idpasangan', // spsm_pasangan.idpasangan   'id'
        'emp_id',
        'emp_id_pasangan',
        'nama_pasangan', // spsm_pasangan.nama_pasangan'   'name'
        's_no_kp_lama', // spsm_pasangan.s_no_kp_lama'   'old_ic_no'
        's_no_kp_baru', // spsm_pasangan.s_no_kp_baru'   'new_ic_no'
        's_tkh_lahir', // spsm_pasangan.s_tkh_lahir'   'date_of_birth'
        's_tkh_kahwin', // spsm_pasangan.s_tkh_kahwin'   'married_at'
        'cukai', // spsm_pasangan.cukai'   'tax'
        'no_lhdn_pasangan', // spsm_pasangan.no_lhdn_pasangan'   'lhdn_no'
        'kod_organisasi', // spsm_pasangan.kod_organisasi'   'organisation_code'
        'jawatan', // spsm_pasangan.jawatan'   'designation'
        'no_tel_r', // spsm_pasangan.no_tel_r'   'home_phone'
        'no_hp', // spsm_pasangan.no_hp'   'mobile_phone'
        'statuspasangan', // spsm_pasangan.statuspasangan'   'status_id'
        'jum_pendapatanbulanan', // spsm_pasangan.jum_pendapatanbulanan'   'total_monthly_income'
        'jum_kwsp', // spsm_pasangan.jum_kwsp'   'total_kwsp'
        'jum_perkeso', // spsm_pasangan.jum_perkeso'   'total_perkeso'
        'jum_cukaipendapatan', // spsm_pasangan.jum_cukaipendapatan'   'total_lhdn'
        'jum_pinjamankenderaan', // spsm_pasangan.jum_pinjamankenderaan'   'total_car_loan'
        'jum_pinjamanperumahan', // spsm_pasangan.jum_pinjamanperumahan'   'total_housing_loan'
        'jum_lain', // spsm_pasangan.jum_lain'   'total_others'
        'telmajikan', // spsm_pasangan.telmajikan'   'employer_phone'
        'tkhstatus', // spsm_pasangan.tkhstatus'   'added_at'
        'status_oku', // spsm_pasangan.status_oku'   'oku'
        'rebat_tax', // spsm_pasangan.rebat_tax'   'total_tax_rebate'
        'bil_isteri', // spsm_pasangan.bil_isteri'   'bil_isteri'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
