<?php

namespace Siza\Database\Models\Spk;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Kodeine\Acl\Traits\HasRole;
use Siza\Database\Models\Spsm\Anak;
use Siza\Database\Models\Spsm\Cuti;
use Siza\Database\Models\Spsm\CutiLayak;
use Siza\Database\Models\Spsm\EmployeeDetail as Detail;
use Siza\Database\Models\Spsm\EmployeeMlain;
use Siza\Database\Models\Spsm\EmployeePendidikan;
use Siza\Database\Models\Spsm\KodDept;
use Siza\Database\Models\Spsm\KodJawatan;
use Siza\Database\Models\Spsm\KodGrade;
use Siza\Database\Models\Spsm\KodUnit;
use Siza\Database\Models\Spsm\Pasangan;
use Siza\Database\Models\Spsm\Pengguna;
use Siza\Database\Models\Spsm\Pertukaran;

/**
 * Class Employee
 * @package Siza\Database\Models\Spk
 * @deprecated Will use \Models\Spsm\Employee instead
 */
class Employee extends Authenticatable //implements AuditableContract
{
    use HasRole, Notifiable; //, Auditable;

    protected $table = 'v2_spk_employees';

    protected $fillable = [
        'id', 'name', 'emp_id', 'email', 'password', 'title',
        'short_name', 'new_ic_no', 'old_ic_no', 'user_name',
        'resign', 'gender', 'first_name', 'last_name', 'auth_key',
        'auth_secret'
    ];

    protected $hidden = [
        'password'
    ];

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return array
     */
    public function getAuthIdentifiersName()
    {
        return ['email', 'emp_id', 'new_ic_no'];
    }

    /**
     * Users can belong to many permissions.
     *
     */
    public function permissions()
    {
        $model = config('acl.permission', Permission::class);

        return $this->belongsToMany($model, 'v2_spk_permission_user', 'employee_id', 'permission_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        $model = config('acl.role', Role::class);

        return $this->belongsToMany($model, 'v2_spk_role_user', 'employee_id', 'role_id')
            ->withTimestamps();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employments()
    {
        return $this->hasMany(Pertukaran::class, 'emp_id', 'emp_id')
            ->orderBy('pertukaran_id', 'desc');
    }

    /**
     * Relationship to SPSM_Pertukaran table
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function employment()
    {
        return $this->hasOne(Pertukaran::class, 'emp_id', 'emp_id')
            ->orderBy('pertukaran_id', 'desc');
    }

    /**
     * @return string
     */
    public function getNameAttribute($value)
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function getJantinaAttribute()
    {
        return $this->detail->jantina == 'L' ? 'Lelaki' : 'Perempuan';
    }

    /**
     * Return user Unit name
     *
     * @return string
     */
    public function getUnitNameAttribute()
    {
        $unit = $this->unit();

        if (! $unit) {
            return false;
        }

        return ucwords(strtolower($unit->butiran));
    }

    /**
     * Return user Department name
     *
     * @return string
     */
    public function getDepartmentNameAttribute()
    {
        $dept = $this->department();

        if (! $dept) {
            return false;
        }

        return ucwords(strtolower($dept->butiran));
    }

    /**
     * @return string
     */
    public function getDesignationNameAttribute()
    {
        $jawatan = KodJawatan::where('kod', $this->employee->jawatan_id)->first();

        if (! $jawatan) {
            return false;
        }

        return ucwords(strtolower($jawatan->butiran));
    }

    /**
     * @return string
     */
    public function getGradeNameAttribute()
    {
        $grade = KodGrade::where('kod', $this->employee->jawatan_id)->first();

        if (! $grade) {
            return false;
        }

        return ucwords(strtolower($grade->butiran));
    }

    /**
     * @return string
     */
    public function getGradeCodeAttribute()
    {
        $kodGrade = KodGrade::where('kod', $this->employee->jawatan_id)->first();

        if (! $kodGrade) {
            return false;
        }

        return $kodGrade->grade;
    }

    /**
     * @return null
     */
    public function unit()
    {
        if (! $this->employee) {
            return false;
        }

        return KodUnit::where('kod', $this->employee->unit_id)->first();
    }

    /**
     * @return null
     */
    public function department()
    {
        if (! $this->employee) {
            return false;
        }

        return KodDept::where('kod', $this->employee->dept_id)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pengguna()
    {
        return $this->belongsTo(Pengguna::class, 'emp_id', 'emp_id')
            ->where('status', 1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function detail()
    {
        return $this->hasOne(Detail::class, 'emp_id', 'emp_id');
    }

    public function jumlahCutiLayak()
    {
        $layak = $this
            ->cutiLayak()
            ->where('tahun_cuti', date('Y'))
            ->first();

        return $layak->cuti_layak;
    }

    public function jumlahCutiTambah()
    {
        $layak = $this
            ->cutiLayak()
            ->where('tahun_cuti', date('Y'))
            ->first();

        return $layak->jum_cuti_tambahan;
    }

    public function jumlahCutiBF()
    {
        $layak = $this
            ->cutiLayak()
            ->where('tahun_cuti', date('Y'))
            ->first();

        return $layak->cuti_bf;
    }

    public function jumlahCuti()
    {
        $layak = $this
            ->cutiLayak()
            ->where('tahun_cuti', date('Y'))
            ->first();

        return $layak->jumlah_cuti;
    }

    public function jumlahBakiCuti()
    {
        $layak = $this
            ->cutiLayak()
            ->where('tahun_cuti', date('Y'))
            ->first();

        return $layak->jumlah_cuti;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    //    public function homeAddress()
    //    {
    //        return $this->hasMany(EmployeeAddress::class)
    //            ->where('home_address', true)
    //            ->orderBy('created_at', 'desc');
    //    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    //    public function permanentAddress()
    //    {
    //        return $this->hasMany(EmployeeAddress::class)
    //            ->where('home_address', false)
    //            ->orderBy('created_at', 'desc');
    //    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pasangan()
    {
        return $this->hasMany(Pasangan::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function anak()
    {
        return $this->hasMany(Anak::class, 'emp_id', 'emp_id')
            ->orderBy('s_tkh_lahir', 'asc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function ibubapa()
    {
        return $this->hasOne(EmployeeMlain::class, 'emp_id', 'emp_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pendidikan()
    {
        return $this->hasMany(EmployeePendidikan::class, 'emp_id', 'emp_id')
            ->orderBy('tahundari', 'desc')
            ->orderBy('tahunhingga', 'desc');
    }

    /**
     * @return string
     */
    public function getAvatarAttribute()
    {
        if (file_exists(storage_path('app/public/profile/' . $this->emp_id . '.png'))) {
            return asset('storage/profile/' . $this->emp_id . '.png', true);
        }
        return asset('storage/profile/default.png', true);
    }

    /**
     * Kelayakan cuti mengikut tahun
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cutiLayak()
    {
        return $this->hasMany(CutiLayak::class, 'emp_id', 'emp_id')
            ->orderBy('tahun_cuti', 'desc');
    }

    /**
     * Senarai cuti yang dimohon
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cuti()
    {
        return $this->hasMany(Cuti::class, 'emp_id', 'emp_id')
            ->orderBy('cuti_id', 'desc');
    }

    public function authKey()
    {
        return $this->auth_key;
    }

    public function authSecret()
    {
        return $this->auth_secret;
    }
}
