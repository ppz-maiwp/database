<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class EmploymentType
 * @package Siza\Database\Models\Spk
 * @deprecated
 */
class EmploymentType extends AbstractModel
{
    protected $table = 'v2_spk_employment_types';

    protected $fillable = [
        'id', 'name'
    ];

    public function employments()
    {
        return $this->hasMany(Employment::class);
    }
}
