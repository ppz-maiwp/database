<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class MaritalStatus
 * @package Siza\Database\Models\Spk
 * @deprecated  Use Siza\Database\Models\Spsm\KodStatusKahwin
 */
class MaritalStatus extends AbstractModel
{
    protected $table = 'v2_spk_marital_statuses';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employeeDetail()
    {
        return $this->hasMany(EmployeeDetail::class);
    }
}
