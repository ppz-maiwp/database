<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class Designation
 * @package Siza\Database\Models\Spk
 * @deprecated  Use Siza\Database\Models\Spsm\KodJawatan
 */
class Designation extends AbstractModel
{
    protected $table = 'v2_spk_designations';

    protected $fillable = [
        'id',
        'name',
        'ordering',
        'scheme_id',
    ];
}
