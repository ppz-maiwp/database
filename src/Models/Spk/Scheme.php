<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class Scheme
 * @package Siza\Database\Models\Spk
 * @deprecated
 */
class Scheme extends AbstractModel
{
    protected $fillable = [
        'year'
    ];
}
