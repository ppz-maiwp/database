<?php

namespace Siza\Database\Models\Spk;

use Siza\Database\Models\AbstractModel;

/**
 * Class EmploymentReason
 * @package Siza\Database\Models\Spk
 * @deprecated
 */
class EmploymentReason extends AbstractModel
{
    protected $table = 'v2_spk_employment_reasons';

    protected $fillable = [
        'id', 'name'
    ];

    public function employments()
    {
        return $this->hasMany(Employment::class);
    }
}
